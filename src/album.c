#include "album.h"
#include "teams.h"
#include <stdio.h>

//Adiciona um player a um album.
void AddPlayer(Album* album, int player){
    album->OwnedPlayers[player] = 1;
    ++album->NumberOfCards;
}

//Remove um jogador de um album.
void RemovePlayer(Album* album, int player){
    album->OwnedPlayers[player] = 0;
    --album->NumberOfCards;
}
